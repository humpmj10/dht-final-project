/*
* Simple linked list implementation, open source code. Made many modifications to handle my data.
*/

#include<stdio.h>
#include<stdlib.h>

#define DEBUG 0
#define BUFFER_SIZE 1000

typedef struct KeyList {
    int key;
    int value;
} KeyList;

struct node
{
    struct KeyList keys;
    struct node *next;
}*head,*var,*trav;

void insert_beg(int key, int value)
{
     var=(struct node *)malloc(sizeof (struct node));
     var->keys.key=key;
     var->keys.value=value;

     if(head==NULL)
     {
         head=var;
         head->next=NULL;
     }
     else
     {
         var->next=head;
         head=var;
     }
}

void insert_inOrder(int key, int value)
{
      struct node *temp; 
      temp=head;
      var=(struct node *)malloc(sizeof (struct node));
      var->keys.key=key;
      var->keys.value=value;

      if(head==NULL)
      {
          head=var;
          head->next=NULL;
      }
      else
      {
          while(temp->next !=NULL && temp->next->keys.key < key)
          {     
               temp=temp->next;
          }
          var->next = temp->next;
          temp->next=var;
      }
}

int delete(int key, int value)
{
  struct node *temp,*var;
  temp=head;
  while(temp!=NULL)
  {
    if(temp->keys.key == key)
    {
      if(temp==head)
      {
       head=temp->next;
       free(temp);
       return 0;
     }
     else
     {
       var->next=temp->next;
       free(temp);
       return 0;
     }
   }
   else
   {
     var=temp;
     temp=temp->next;
   }
 }

}

int getValue( int key )
{
  trav=head;
  if(trav==NULL)
  {
    if ( DEBUG )
      printf("List is Empty\n");
  }
  else
  {
    while(trav!=NULL)
    {
      if ( trav->keys.key == key)
        return trav->keys.value;
      else
        trav=trav->next;
    }
      if ( DEBUG )
        printf("Key %d is not in the list\n", key);
  }
}

getKeysAll(int *array)
{
  trav=head;
  int i = 2; // counter for index in the array
  int numberOfPairs = 0; // will hold the number of pairs found

  if(trav == NULL)
  {
    if ( DEBUG )
      printf("List is Empty\n");
  }
  else
  {
    while(trav != NULL)
      { 
        array[i] = trav->keys.key;
        array[i+1] = trav->keys.value;
        i+=2;
        numberOfPairs++;
        trav=trav->next;
      }
  }
  array[0] = numberOfPairs;
}

void clearList()
{
  head=NULL;
  trav=NULL;
  var=NULL;
  
} /* freeList */


void display()
{
  trav=head;
  if(trav==NULL)
  {
    printf("List is Empty");
  }
  else
  {
    printf("\nElements in the List: ");
    while(trav!=NULL)
    {
      printf( " -> key=%d value=%d",trav->keys.key, trav->keys.value );
      trav=trav->next;
    }
      printf("\n");
    }
}