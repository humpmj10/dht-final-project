MPICC  = mpicc


dht: dht.o linkedList.o
	${MPICC} -o dht dht.o linkedList.o

dht.o: dht.c
	${MPICC} -c dht.c

linkedList.o: linkedList.c
	${MPICC} -c linkedList.c

clean:
	/bin/rm *.o