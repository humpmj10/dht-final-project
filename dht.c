/*
*	Programmed by Michael Humphrey
*	CSC 422 Lowenthal
*	This program is a simpliefied version of a DHT. The program will use MPI to implement the dht.
*	One head node will be created to handle commands and start distribution of information and messages.
*	The dht will consists of singly linked nodes that will store keys. The program will be able to
*	handle 5 different commands.
*	
*	PUT: 0 <key> <value>: stores a value on one of the storage nodes.
*	GET: 1 <key>: retrieves the value from the correct node and prints it on the head node.
* 	ADD: 2 <rank> <id>: adds the node of the speciﬁed rank to the DHT, giving it
*	the speciﬁed id (to be used for determining which keys it will store).
* 	REMOVE: 3 <id>: removes the node with the speciﬁed id from the DHT.
*	END: 4: terminates the program.
*
*	The head node, Rank 0, will handle these commands. With the exception of end, it will pass the messages to its child
*	and they will pass it on until the appropriate node recieves the command. Once it recieves the command an acknowledge message
*	will be sent around back to the head node. The head node will not process any addition commands until it recieves the ack message.
*	So only one command can be processed at a time.
*/



#include <stdlib.h>
#include <stdio.h>
#include "mpi.h"
#include <string.h>
#include <limits.h>

#define BUFFER_SIZE 1000
#define DEBUG 0

int main ( int argc, char **argv )
{
	int status, gstatus, i, j, k;
    int np, me;
    int size;
    int key; // will store the key given in put and get commands
    int value; // will store the value given in put command
    int rank; // will store the rank, used when creating new nodes
    int myID; // will store the ID assigned to node when it's created
    int id; // used in message passing
	int command; // will hold the commands read from stdin
    int nextNodeRank = -1; // childNode of the current node
    int nextNodeID = -1; // the childNode's ID given by add command
    int prevNodeRank = -1; // childNode of the current node
    int prevNodeID = -1; // the childNode's ID given by add command
    int active = 0; // determine if the node is active, 0 = inactive, 1 = active
    int *sendBuffer; // will be used to malloc space for send buffers
    int *recvBuffer; // will be used to malloc space for recv buffers
    int messageFlag; // used to see if any messages are waiting on the comm
    int endFlag;
    int numKeys = 0; // the number of keys a node has
    struct node *keyListHead; // will hold the keys for each worker
    int myKeys[BUFFER_SIZE] = {0}; // used for sending keys to other nodes
    int keyCount; // used to count number of keys being passed over to another node

    MPI_Status mpiStatus; 

    /* MPI init */
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD,&me);
    MPI_Comm_size(MPI_COMM_WORLD,&np);

    size = np;
    
    if ( DEBUG )
        printf( "My rank is %d, and my nextNode is %d\n", me, nextNodeRank );

    if ( me == 0 ) 
    {
        active = 1; // activate the head node

    	while( 1 )
    	{
    		scanf( "%d", &command );

            sendBuffer = malloc( sizeof(int) * BUFFER_SIZE );
            recvBuffer = malloc( sizeof(int) * BUFFER_SIZE );

    		if ( 0 == command ) // PUT command
    		{
                // small error check to make sure nodes are there for storage
                if ( -1 != nextNodeRank && -1 != nextNodeID )
                {
    			    scanf( "%d%d", &key, &value ); 
    			    if ( DEBUG )
    				    printf( "Recieved a PUT command key = %d, value = %d\n", key, value );

                    sendBuffer[0] = command;
                    sendBuffer[1] = key;
                    sendBuffer[2] = value;

                    status = MPI_Send( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD ); // send message to the next node

                    status = MPI_Recv( recvBuffer, BUFFER_SIZE, MPI_INT, prevNodeRank, me, MPI_COMM_WORLD, &mpiStatus ); // wait for ack message

                     if ( DEBUG )
                    {
                        printf( "Recieved ack PUT message, it was %d\n", recvBuffer[0] );
                    }
                }
                else if ( DEBUG )
                {
                    printf("No available nodes to store data\n");
                }

    		}
    		else if ( 1 == command ) // GET command
    		{
                // small error check to make sure nodes are there for storage
                if ( -1 != nextNodeRank && -1 != nextNodeID )
                {
    			    scanf( "%d", &key ); 
    			    if ( DEBUG )
    				    printf( "Recieved a GET command key = %d\n", key );

                    sendBuffer[0] = command;
                    sendBuffer[1] = key;

                    status = MPI_Send( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD ); // send message to the next node

                    status = MPI_Recv( recvBuffer, BUFFER_SIZE, MPI_INT, prevNodeRank, me, MPI_COMM_WORLD, &mpiStatus ); // wait for ack message
                    if ( DEBUG )
                    {
                        printf( "Recieved ack GET message, it was %d\n", recvBuffer[0] );
                    }
                    printf("%d %d\n", recvBuffer[1], recvBuffer[2]); // print the value forwarded to head
                }
                else if ( DEBUG )
                {
                    printf("No available nodes to store data\n");
                }
    		}
    		else if ( 2 == command ) // ADD command
    		{
    			scanf( "%d%d", &rank, &id ); 

    			if ( DEBUG )
    				printf( "Recieved a ADD command rank = %d, id = %d\n", rank, id );
                
                sendBuffer[0] = command;
                sendBuffer[1] = rank;
                sendBuffer[2] = id;

                // no nodes have been intialized yet
                if ( -1 == nextNodeRank || 0 == nextNodeRank )
                {
                    // first node that has been added both next and prev will be this node
                    nextNodeRank = rank;
                    nextNodeID = id;
                    prevNodeRank = rank;
                    prevNodeID = id;

                    // meta info needed to keep list in order
                    sendBuffer[3] = me; // prev node rank
                    sendBuffer[4] = me; // next node rank
                    sendBuffer[5] = myID;
                    sendBuffer[6] = myID;

                    status = MPI_Send( sendBuffer, BUFFER_SIZE, MPI_INT, rank, rank, MPI_COMM_WORLD ); // send message to the next node

                    status = MPI_Recv( recvBuffer, BUFFER_SIZE, MPI_INT, rank, me, MPI_COMM_WORLD, &mpiStatus ); // wait for ack message
                }
                // special case where the node is being inserted right after head
                else if ( id < nextNodeID )
                {

                    // meta info needed to keep list in order
                    sendBuffer[3] = me; // prev node rank
                    sendBuffer[4] = nextNodeRank; // next node rank
                    sendBuffer[5] = myID; // prev node ID
                    sendBuffer[6] = nextNodeID; // next node ID

                    // update head with new rank and ID, the one being created
                    nextNodeRank = rank;
                    nextNodeID = id;

                    status = MPI_Send( sendBuffer, BUFFER_SIZE, MPI_INT, rank, rank, MPI_COMM_WORLD ); // send message to the next node

                    status = MPI_Recv( recvBuffer, BUFFER_SIZE, MPI_INT, MPI_ANY_SOURCE, me, MPI_COMM_WORLD, &mpiStatus ); // wait for ack message
                    prevNodeRank = recvBuffer[1]; // update the new prev node rank
                    prevNodeID = recvBuffer[2]; // update the new prev node id
                }
                else
                {
                    status = MPI_Send( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD ); // send message to the next node

                    status = MPI_Recv( recvBuffer, BUFFER_SIZE, MPI_INT, MPI_ANY_SOURCE, me, MPI_COMM_WORLD, &mpiStatus ); // wait for ack message
                    prevNodeRank = recvBuffer[1]; // update the new prev node rank
                    prevNodeID = recvBuffer[2]; // update the new prev node id
                }
                if ( DEBUG )
                {
                    printf( "Recieved ack ADD message, it was %d\n", recvBuffer[0] );
                }
    		}
    		else if ( 3 == command ) // REMOVE command
    		{
    			scanf( "%d", &id ); 
    			if ( DEBUG )
    				printf( "Recieved a REMOVE command id = %d\n", id );

                /* Malloc Space for buffer and add in rank and id */
                sendBuffer = malloc( sizeof(int) * BUFFER_SIZE );
                sendBuffer[0] = command;
                sendBuffer[1] = id;

                status = MPI_Send( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD ); // send message to the next node

                // if the node being deleted is right after or before head, special message is sent to get new node links
                if ( id == nextNodeID )
                {
                    status = MPI_Recv( recvBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, me, MPI_COMM_WORLD, &mpiStatus ); // wait for ack message
                    nextNodeRank = recvBuffer[1];
                    nextNodeID = recvBuffer[2];

                    if ( DEBUG )
                        printf("Head recieved updated node info nextNodeRank=%d, nextNodeID=%d\n", nextNodeRank, nextNodeID);
                }
                if ( id == prevNodeID  )
                {
                    status = MPI_Recv( recvBuffer, BUFFER_SIZE, MPI_INT, prevNodeRank, me, MPI_COMM_WORLD, &mpiStatus ); // wait for ack message
                    prevNodeRank = recvBuffer[1];
                    prevNodeID = recvBuffer[2];

                     if ( DEBUG )
                        printf("Head recieved updated node info prevNodeRank=%d, prevNodeID=%d\n", prevNodeRank, prevNodeID);
                }


                status = MPI_Recv( recvBuffer, BUFFER_SIZE, MPI_INT, MPI_ANY_SOURCE, me, MPI_COMM_WORLD, &mpiStatus ); // wait for ack message
                if ( DEBUG )
                {
                    printf( "Recieved ack REMOVE message, it was %d\n", recvBuffer[0] );
                }

    		}
    		else if ( 4 == command) // END command
    		{
    			if ( DEBUG )
    				printf( "Recieved an END command exiting the program\n" );
                for ( i = 1; i < size; i++ )
                {
                    sendBuffer = malloc( sizeof(int) * BUFFER_SIZE );
                    sendBuffer[0] = command;
                    status = MPI_Send(sendBuffer, BUFFER_SIZE, MPI_INT, i, i, MPI_COMM_WORLD); // send message to the next node
                }
                free( sendBuffer );
                free( recvBuffer );
    			break;
    		}
            // command for debugging
            else if ( 8 == command )
            {
                if ( DEBUG )
                {
                    printf( "I'm the head nextNodeRank=%d, nextNodeID=%d, prevNodeRank=%d, prevNodeID=%d\n", nextNodeRank, nextNodeID, prevNodeRank, prevNodeID );
                }
                sendBuffer[0] = 8;
                status = MPI_Send( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD ); // send message to the next node

                status = MPI_Recv( recvBuffer, BUFFER_SIZE, MPI_INT, MPI_ANY_SOURCE, me, MPI_COMM_WORLD, &mpiStatus ); // wait for ack message
                if ( DEBUG )
                {
                    printf( "Recieved ack DETAIL message, it was %d\n", recvBuffer[0] );
                }
            }

            /* Free Buffers */
            free( sendBuffer );
            free( recvBuffer );

    	}
    }
    // worker loops
    else
    {
        while ( 1 )
        {
            // see if any messages are in the comm
            status = MPI_Iprobe( MPI_ANY_SOURCE, me, MPI_COMM_WORLD, &messageFlag, &mpiStatus ); // listen for previous node
            status = MPI_Iprobe( 0, me, MPI_COMM_WORLD, &endFlag, &mpiStatus ); // listen for head node, in case of END

            if ( messageFlag || endFlag)
            {
                sendBuffer = malloc ( sizeof(int) * BUFFER_SIZE );
                recvBuffer = malloc ( sizeof(int) * BUFFER_SIZE );

                if ( messageFlag ) // regular message
                {
                    status = MPI_Recv( recvBuffer, BUFFER_SIZE, MPI_INT, MPI_ANY_SOURCE, me, MPI_COMM_WORLD, &mpiStatus ); // take message from comm
                }
                else // might be an end message
                {
                    status = MPI_Recv( recvBuffer, BUFFER_SIZE, MPI_INT, 0, me, MPI_COMM_WORLD, &mpiStatus ); // take message from comm
                }

                command = recvBuffer[0];
                sendBuffer[0] = command;

                if ( DEBUG )
                    printf("My rank is %d, recieved a message, it was a %d command\n", me, command );

                if ( 0 == command ) // PUT command
                {
                    key = recvBuffer[1];
                    value = recvBuffer[2];

                    if ( nextNodeRank == 0 || myID >= key )
                    {
                        insert_beg(key, value);
                        sendBuffer[0] = 9;
                        numKeys++;
                        status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD); // send ack message
                    }
                    else
                    {
                        sendBuffer[0] = command;
                        sendBuffer[1] = key;
                        sendBuffer[2] = value;
                        status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD); // send message to the next node w/ request
                    }
                }
                else if ( 1 == command ) // GET command
                {
                    key = recvBuffer[1];
                    // check to see if key would exist on this node, if yes than get value
                    if ( nextNodeRank == 0 || myID >= key )
                    {
                        value = getValue( key );
                        if ( DEBUG )
                        {
                            printf("Found the value on node %d it was %d\n", me, value);
                        }
                        sendBuffer[0] = 9;
                        sendBuffer[1] = value;
                        sendBuffer[2] = myID;
                        status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD); // send message to the next node w/ request
                    }
                    // send message to next node for value
                    else
                    {
                        sendBuffer[0] = command;
                        sendBuffer[1] = key;
                        status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD); // send message to the next node w/ request
                    }
                }
                else if ( 2 == command ) // ADD command
                {
                    rank = recvBuffer[1];
                    id = recvBuffer[2];

                    if ( DEBUG && me != rank )
                    {
                        printf("I am process %d. Recieved an add message, it's not me, forwarding to node %d\n", me, nextNodeRank);
                    }
                    // I'm the node, activate me
                    if ( me == rank )
                    {

                        myID = id;
                        active = 1;
                        prevNodeRank = recvBuffer[3];
                        prevNodeID = recvBuffer[5];
                        nextNodeRank = recvBuffer[4];
                        nextNodeID = recvBuffer[6];

                        // send updated list info
                        if ( 0 != nextNodeRank )
                        {
                            // send the node after insertion the updated prev node info
                            sendBuffer[0] = 7;
                            sendBuffer[1] = me;
                            sendBuffer[2] = myID;
                            status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD); // send message to the next node 

                            // see if larger node had any keys
                            sendBuffer[0] = 12;
                            sendBuffer[1] = me;
                            sendBuffer[2] = myID;
                            status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD); // send message to next asking for keys

                            status = MPI_Recv ( recvBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, me, MPI_COMM_WORLD, &mpiStatus); // send message to next asking for keys
                            j = 2;
                            // iterate over the key/values and insert into this nodes list
                            for ( i = 0; i < recvBuffer[1]; i++ )
                            {
                                if (DEBUG)
                                    printf("inserting key =%d, value=%d", recvBuffer[j], recvBuffer[j+1]);
                                insert_beg(recvBuffer[j], recvBuffer[j+1]);
                                numKeys++;
                                j+=2;
                            }
                            if (DEBUG)
                                display();
                        }
                        // need to see if previous largest node had keys to pass over
                        else
                        {
                            // make sure prev node not the head and the ask for keys
                            if ( 0 != prevNodeRank )
                            {
                                sendBuffer[0] = 11;
                                sendBuffer[1] = me;
                                sendBuffer[2] = myID;
                                status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, prevNodeRank, prevNodeRank, MPI_COMM_WORLD); // send message to next asking for keys

                                status = MPI_Recv ( recvBuffer, BUFFER_SIZE, MPI_INT, prevNodeRank, me, MPI_COMM_WORLD, &mpiStatus); // get keys back
                                j = 2;
                                // iterate over the key/values and insert into this nodes list
                                for ( i = 0; i < recvBuffer[1]; i++ )
                                {
                                    if (DEBUG)
                                        printf("inserting key =%d, value=%d", recvBuffer[j], recvBuffer[j+1]);
                                    insert_beg(recvBuffer[j], recvBuffer[j+1]);
                                    numKeys++;
                                    j+=2;
                                }
                                if (DEBUG)
                                    display();
                            }
                        }

                        sendBuffer[0] = 5;
                        if ( DEBUG )
                        {
                            printf("I am process %d. Recieved an add message, activating and sending ack\n", me);
                        }
                    }

                    sendBuffer[1] = rank;
                    sendBuffer[2] = id;
                    sendBuffer[3] = me; // prev node rank
                    sendBuffer[4] = nextNodeRank; // next node rank
                    sendBuffer[5] = myID; // prev node ID
                    sendBuffer[6] = nextNodeID; // next node's ID

                    // if the node's id is greater or the next node is the head node, send message to node to add itself 
                    if ( id > myID && id < nextNodeID || ( nextNodeRank == 0 && sendBuffer[0] != 5 ))
                    {
                        nextNodeRank = rank;
                        nextNodeID = id;
                        status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, rank, rank, MPI_COMM_WORLD); // send message to node to activate it
                    }
                    else
                    {
                        status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD); // send message to the next node
                    }
                }
                else if ( 3 == command ) // REMOVE command
                {
                    id = recvBuffer[1];

                    // I'm the node needing to be removed
                    if ( id == myID )
                    {

                        if ( DEBUG )
                        {
                            printf("I am process %d. Recieved an remove message, removing myself and updating links\n", me);
                        }

                        sendBuffer[0] = 6;

                        // send updated link info to prevNode
                        sendBuffer[1] = nextNodeRank;
                        sendBuffer[2] = nextNodeID;
                        sendBuffer[3] = id;
                        status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, prevNodeRank, prevNodeRank, MPI_COMM_WORLD); // send message to the prev node w/ updated list for prev
                        if ( prevNodeRank != 0 )
                            status = MPI_Recv( recvBuffer, BUFFER_SIZE, MPI_INT, prevNodeRank, me, MPI_COMM_WORLD, &mpiStatus ); // take message from comm
                        
                        // send updated link info to nextNode
                        sendBuffer[1] = prevNodeRank;
                        sendBuffer[2] = prevNodeID;
                        sendBuffer[3] = id;
                        status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD); // send message to the next node w/ updated list for next
                        if ( nextNodeRank != 0 )
                            status = MPI_Recv( recvBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, me, MPI_COMM_WORLD, &mpiStatus ); // take message from comm

                        // if the the deleted node was the last node, send all keys to the prevNode
                        if ( 0 == nextNodeRank )
                        {
                            // check to make sure this isn't the only node, if it is no keys will be exchanged
                            if ( 0 != prevNodeRank )
                            {
                                // check to make sure node had keys
                                if ( numKeys != 0 )
                                {
                                    getKeysAll( &myKeys );  // get all the keys being stored for this node
                                    if ( DEBUG  )
                                    {
                                        printf("Node %d, I found %d keys to xfer. Sending to node %d\n", me, myKeys[0], prevNodeID);
                                    }
                                    
                                    j = 2;
                                    for ( i = 0; i < myKeys[0]; i++ )
                                    {
                                        sendBuffer[j+1] = myKeys[j]; // the key
                                        sendBuffer[j+2] = myKeys[j+1]; // the value
                                        j+=2;
                                    }

                                    sendBuffer[0] = 10;
                                    sendBuffer[1] = myKeys[0]; // send the number of keys being transfered
                                    sendBuffer[2] = me; // send the number of keys being transfered
                                    status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, prevNodeRank, prevNodeRank, MPI_COMM_WORLD); // send message to the next node w/ updated list for next

                                    status = MPI_Recv ( recvBuffer, BUFFER_SIZE, MPI_INT, prevNodeRank, me, MPI_COMM_WORLD, &mpiStatus); // send message to the next node w/ updated list for next
                                }
                                
                            }
                        }
                        // all the keys have to go to the next node, they must be larger than prevNode and smaller than nextNode
                        else
                        {
                            // check to make sure node had keys, if so put them in send buffer and send over
                            if ( numKeys != 0 )
                            {
                                
                                getKeysAll( &myKeys );

                                if ( DEBUG )
                                {
                                    printf("Node %d, I found %d keys to xfer. Sending to node %d\n", me, myKeys[0], nextNodeID);
                                }
                                j = 2;
                                for ( i = 0; i < myKeys[0]; i++ )
                                {
                                    sendBuffer[j+1] = myKeys[j]; // the key
                                    sendBuffer[j+2] = myKeys[j+1]; // the value
                                    j+=2;
                                }
                                sendBuffer[0] = 10;
                                sendBuffer[1] = myKeys[0]; // send the number of keys being transfered
                                sendBuffer[2] = me;
                                status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD); // send message to the next node w/ updated list for next
                                
                                status = MPI_Recv ( recvBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, me, MPI_COMM_WORLD, &mpiStatus); // send message to the next node w/ updated list for next
                            }
                        }

                        // after sending out keys, send ack message to next node
                        sendBuffer[0] = 5;
                        sendBuffer[1] = id;
                        status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD); // send ack message
                        if ( DEBUG )
                        {
                            printf("I am process %d. Recieved an remove message, deactivating and sending ack\n", me);
                        }

                        // reset the node's info
                        myID = -1;
                        active = 0;
                        clearList();
                        keyListHead = NULL;
                        prevNodeRank = -1;
                        prevNodeID = -1;
                        numKeys = 0;
                    }
                    else
                    {
                        if ( DEBUG )
                        {
                            printf("I am process %d. Recieved an remove message, it's not me, forwarding to node %d\n", me, nextNodeRank);
                        }
                        sendBuffer[0] = command;
                        sendBuffer[1] = id;
                        status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD ); // send message to the next node 
                    }
                }
                else if ( 4 == command ) // END command
                {
                    free( sendBuffer );
                    free( recvBuffer );
                    break;
                }
                else if ( 5 == command ) // forward ack message to head
                {
                    sendBuffer[0] = 5;
                    sendBuffer[1] = me;
                    sendBuffer[2] = myID;

                    status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD ); // send message to the next node 
                }
                else if ( 6 == command ) // give new updated list info after deletion
                {

                    id = recvBuffer[3];

                    // see if it was next or prev that sent the message to update links
                    if ( nextNodeID == id )
                    {
                        status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD ); // send message to the next node 
                        nextNodeRank = recvBuffer[1];
                        nextNodeID = recvBuffer[2];
                        if ( DEBUG )
                        {
                            printf("I am process %d. Recieved update link message. nextNodeRank=%d, nextNodeID=%d\n", me, nextNodeRank, nextNodeID);
                        }
                    }
                    else if ( prevNodeID == id ) 
                    {
                        status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, prevNodeRank, prevNodeRank, MPI_COMM_WORLD ); // send message to the next node 
                        prevNodeRank = recvBuffer[1];
                        prevNodeID = recvBuffer[2];
                        if ( DEBUG )
                        {
                            printf("I am process %d. Recieved update link message. prevNodeRank=%d, prevNodeID=%d\n", me, prevNodeRank, prevNodeID);
                        }

                    }
                }
                else if ( 7 == command ) // give info to update prev when an node is added
                {
                    prevNodeRank = recvBuffer[1];
                    prevNodeID = recvBuffer[2];
                }
                else if ( 8 == command ) // debug info
                {
                    printf("My rank =%d, nextNodeID=%d, nextNodeRank=%d, prevNodeID=%d, prevNodeRank=%d\n", me, nextNodeID, nextNodeRank, prevNodeID, prevNodeRank);
                    if (numKeys != 0)
                        display();
                    status = MPI_Send ( sendBuffer, 3, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD); // send message to the next node 
                }
                else if ( 9 == command ) // get ack message for get/put
                {
                    sendBuffer[0] = 9;
                    sendBuffer[1] = recvBuffer[1]; // the key
                    sendBuffer[2] = recvBuffer[2]; // the id from node who retreived the key

                    status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, nextNodeRank, nextNodeRank, MPI_COMM_WORLD); // send message to the next node 
                }
                else if ( 10 == command ) // message that contains keys to add to the node
                {
                    j = 3;
                    // iterate over the key/values and insert into this nodes list
                    for ( i = 0; i < recvBuffer[1]; i++ )
                    {
                        if (DEBUG)
                            printf("inserting key =%d, value=%d", recvBuffer[j], recvBuffer[j+1]);
                        insert_beg(recvBuffer[j], recvBuffer[j+1]);
                        numKeys++;
                        j+=2;
                    }
                    if (DEBUG)
                        display();
                    status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, recvBuffer[2], recvBuffer[2], MPI_COMM_WORLD ); // send ack message back 
                }
                else if ( 11 == command ) // ask if any keys need to passed over
                {
                    
                        keyCount = 0;
                        getKeysAll( &myKeys );
                    
                        j = 2;
                        k = 2;
                        for ( i = 0; i < myKeys[0]; i++ )
                        {
                            // check if the key belongs with node, if so add to buffer
                            if ( myKeys[j] > myID )
                            {
                                sendBuffer[k] = myKeys[j]; // the key
                                sendBuffer[k+1] = myKeys[j+1]; // the value
                                delete( myKeys[j], myKeys[j+1] );
                                numKeys--;
                                k+=2;
                                keyCount++;
                            }
                            j+=2;
                        }
                        sendBuffer[0] = 10;
                        sendBuffer[1] = keyCount; // let node know how many keys are coming over
                        status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, recvBuffer[1], recvBuffer[1], MPI_COMM_WORLD); // send message to the next node w/ updated list for next

                    
                }
                else if ( 12 == command ) 
                {
                    
                        keyCount = 0;
                        getKeysAll( &myKeys );
                    
                        j = 2;
                        k = 2;
                        for ( i = 0; i < myKeys[0]; i++ )
                        {
                            // check to see if keys belong on the node, if so add to buffer
                            if ( myKeys[j] <= recvBuffer[2] )
                            {
                                sendBuffer[k] = myKeys[j]; // the key
                                sendBuffer[k+1] = myKeys[j+1]; // the value
                                delete( myKeys[j], myKeys[j+1] );
                                numKeys--;
                                k+=2;
                                keyCount++;
                            } 
                            j+=2;
                        }
                        sendBuffer[0] = 10;
                        sendBuffer[1] = keyCount;
                        status = MPI_Send ( sendBuffer, BUFFER_SIZE, MPI_INT, recvBuffer[1], recvBuffer[1], MPI_COMM_WORLD); // send message to the next node w/ updated list for next
                        

                    
                }
                free( sendBuffer );
                free( recvBuffer );
            }
        }
    }
    MPI_Finalize();

    return 0;
} /* end main */









